using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SC_Line))]
public class SC_LineInspector : Editor
{
	private void OnSceneGUI()
	{
		SC_Line line = target as SC_Line; //the target is the obj we want to draw when OnSceneGUI is called

		//converts to absolute to world points so the line can properly follow the paddles
		Transform handleTransform = line.transform;
		Quaternion handleRotation = Tools.pivotRotation == PivotRotation.Local ?handleTransform.rotation : Quaternion.identity;
		Vector3 p0 = handleTransform.TransformPoint(line.p0);
		Vector3 p1 = handleTransform.TransformPoint(line.p1);
		//
		Handles.color = Color.white;
		Handles.DrawLine(line.p0, line.p1); //handles draws the line
		Handles.DoPositionHandle(p0, handleRotation);
		Handles.DoPositionHandle(p1, handleRotation);

		//assign their results back to the line.
		EditorGUI.BeginChangeCheck();
		p0 = Handles.DoPositionHandle(p0, handleRotation);
		if (EditorGUI.EndChangeCheck())
		{
			Undo.RecordObject(line, "Move Point");//to record when i make a change so i can cntrl-z
			EditorUtility.SetDirty(line);//to say that i made a change
			line.p0 = handleTransform.InverseTransformPoint(p0);
		}
		EditorGUI.BeginChangeCheck();
		p1 = Handles.DoPositionHandle(p1, handleRotation);
		if (EditorGUI.EndChangeCheck()) //reads the change in BeginChangeCheck()
		{
			Undo.RecordObject(line, "Move Point");
			EditorUtility.SetDirty(line);
			line.p1 = handleTransform.InverseTransformPoint(p1);
		}
	}


}
